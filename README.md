# magnesium

![magnesium](images/magnesium.png)

a streaming music player that plays content from youtube

[homepage](https://robblue2x.gitlab.io/magnesium/)

# developers/hackers

## using git

### prerequsites

-   node
-   npm

on ubuntu/debian:

```bash
sudo apt install nodejs npm nodejs-legacy
```

### clone repository

```bash
cd ~/projects
git clone https://gitlab.com/robblue2x/magnesium.git
cd magnesium
npm i
```

### build ui

```bash
cd ~/projects/magnesium
npm run webpack
```

### run program

```bash
cd ~/projects/magnesium
npm start
```

### update

```bash
cd ~/projects/magnesium
git pull
npm i
```
