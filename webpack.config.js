const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
    control: './src/ui/control/index.jsx',
    library: './src/ui/library/index.jsx',
    vendor: ['react', 'semantic-ui-react'],
  },
  target: 'electron-renderer',
  mode: 'development',

  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-react', '@babel/preset-env'],
        },
      },
    ],
  },

  plugins: [
    new CleanWebpackPlugin(),

    new HtmlWebpackPlugin({
      title: 'magnesium',
      filename: 'control.html',
      chunks: ['control', 'vendor'],
      template: 'src/ui/template.ejs',
    }),
    new HtmlWebpackPlugin({
      title: 'magnesium',
      filename: 'library.html',
      chunks: ['library', 'vendor'],
      template: 'src/ui/template.ejs',
    }),
  ],

  resolve: {
    extensions: ['.js', '.jsx'],
  },

  output: {
    path: path.join(__dirname, 'ui'),
    filename: '[name].min.js',
  },
};
