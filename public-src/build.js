const pug = require('pug');
const fs = require('fs-extra');

const pkg = require('../package.json');

fs.emptyDirSync('public');

fs.copySync('images', 'public/images');
fs.copySync('assets', 'public/icons');

const out = pug.renderFile(`${__dirname}/index.pug`, {
  version: pkg.version
});

fs.writeFileSync('public/index.html', out);
