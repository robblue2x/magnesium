const { EventEmitter } = require('events');
const { ipcRenderer } = require('electron');

const Broker = new EventEmitter();

Broker.internalEmit = Broker.emit;

Broker.emit = (name, ...args) => {
  console.log(`event - ${name}`, ...args);
  ipcRenderer.send('ui', name, ...args);
};

ipcRenderer.on('main', (e, name, ...args) => {
  console.log(`emit - ${name}`, ...args);
  Broker.internalEmit(name, ...args);
});

const cleanTitle = (t) =>
  t
    .toLowerCase()
    .replace(/<\/?b>/g, '')
    .trim()
    .replace(/[“”()[\]|]/g, '')
    .replace(/deleted video$/g, '')
    .replace(/official music video$/g, '')
    .replace(/ music video$/g, '')
    .replace(/ video$/g, '')
    .replace(/ official/g, '')
    .replace(/ audio$/g, '')
    .replace(/ lyrics?$/g, '')
    .replace(/\s\s+/g, ' ');

const list = [];
for (const e of document.getElementsByTagName('span')) {
  if (e.id === 'video-title') {
    list.push(cleanTitle(e.title));
  }
}

Broker.emit('playlistresults', list);
