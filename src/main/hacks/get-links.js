const { EventEmitter } = require('events');
const { ipcRenderer } = require('electron');

const Broker = new EventEmitter();

Broker.internalEmit = Broker.emit;

Broker.emit = (name, ...args) => {
  console.log(`event - ${name}`, ...args);
  ipcRenderer.send('ui', name, ...args);
};

ipcRenderer.on('main', (e, name, ...args) => {
  console.log(`emit - ${name}`, ...args);
  Broker.internalEmit(name, ...args);
});

const htmlElements = document.getElementsByTagName('a');
const results = [];

for (const he of htmlElements) {
  results.push(he.href);
}

const list = results.filter((l) => l.startsWith('https://www.youtube.com/watch?v='));

Broker.emit('searchresult', list[0]);
