const { EventEmitter } = require('events');
const { ipcRenderer } = require('electron');

const Broker = new EventEmitter();

let video;
let timeout;
let currentTime = 0;

Broker.internalEmit = Broker.emit;

Broker.emit = (name, ...args) => {
  // console.log(`event - ${name}`, ...args);
  ipcRenderer.send('ui', name, ...args);
};
ipcRenderer.on('main', (e, name, ...args) => {
  // console.log(`emit - ${name}`, ...args);
  Broker.internalEmit(name, ...args);
});
Broker.emit('progress', 0, 0);
Broker.on('seek', (t) => {
  if (video) video.currentTime = t;
});

function removeSiblings(a) {
  const dad = a.parentElement;
  while (dad.children.length > 1) {
    for (const c of dad.children) {
      if (c !== a) {
        dad.removeChild(c);
      }
    }
  }
}

const resetTimeout = () => {
  clearTimeout(timeout);
  timeout = setTimeout(() => {
    console.log('timeout waiting for video');
    Broker.emit('next');
  }, 10000);
};
resetTimeout();

setInterval(() => {
  document.title = 'magnesium';
  document.body.className = '';
  document.body.style = {};

  [video] = document.getElementsByTagName('video');
  const ads =
    !!document.getElementsByClassName('ytp-ad-text')[0] ||
    !!document.getElementsByClassName('videoAdUi')[0] ||
    !!document.getElementsByClassName('ad-interrupting')[0];
  if (video) {
    if (ads) {
      video.muted = true;
      video.pause();
      video.currentTime = video.duration;
    } else {
      video.muted = false;
      if (video.currentTime > video.duration - 3) {
        Broker.emit('next');
      }
      if (currentTime !== video.currentTime) {
        ({ currentTime } = video);
        Broker.emit('progress', currentTime, video.duration);
        resetTimeout();
      }
      document.body.appendChild(video);
      removeSiblings(video);

      video.play();
      video.style = {};
      video.style.width = '100%';
      video.style.height = '100%';
      video.style.background = 'black';
    }
  }
}, 500);

window.onkeypress = (e) => {
  if (['q', 'f', 'x', ' '].includes(e.key)) {
    window.close();
  }
};

// nothing playing
if (window.location.href.startsWith('file://')) {
  Broker.emit('progress', 0, 0);
  clearTimeout(timeout);
}

(() => {
  const blank = document.createElement('div');
  blank.style.top = 0;
  blank.style.bottom = 0;
  blank.style.left = 0;
  blank.style.right = 0;
  blank.style.position = 'absolute';
  blank.style.background = 'black';
  blank.style.zIndex = 9999;
  document.body.append(blank);
})();
