const { globalShortcut } = require('electron');
const Broker = require('./Broker');

const construct = () => {
  globalShortcut.register('MediaNextTrack', () => {
    Broker.emit('next');
  });
  globalShortcut.register('MediaStop', () => {
    Broker.emit('stop');
  });
  globalShortcut.register('MediaPlayPause', () => {
    Broker.emit('stop');
  });
};

Broker.on('program:start', construct);
