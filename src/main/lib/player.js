const fs = require('fs-extra');
const Broker = require('./Broker');
const windowManager = require('./window-manager');

let tvModeEnabled = false;
let showPlayer = false;

const hacks = fs.readFileSync(`${__dirname}/../hacks/player-hacks.js`).toString();

const updatePlayerMode = () => {
  if (showPlayer) {
    windowManager.show('player');
  } else {
    windowManager.hide('player');
  }
  windowManager.fullscreen('player', false);
  windowManager.fullscreen('player', tvModeEnabled);
};

const addHacks = () => {
  windowManager.execute('player', hacks);
  windowManager.insertCSS('player', 'html,body{ overflow: hidden !important; }');
};

const show = () => {
  showPlayer = true;
  tvModeEnabled = false;
  updatePlayerMode();
};
const tvmode = () => {
  tvModeEnabled = true;
  showPlayer = tvModeEnabled;
  updatePlayerMode();
};

const load = (vu) => {
  windowManager.load('player', vu);
};

const onHide = () => {
  if (showPlayer) {
    showPlayer = false;
    tvModeEnabled = false;
    updatePlayerMode();
  }
};

Broker.on('show-player', show);
Broker.on('show-tvmode', tvmode);
Broker.on('load', load);
Broker.on('content:player:dom-ready', addHacks);
Broker.on('content:player:did-start-loading', addHacks);
Broker.on('window:player:hide', onHide);
