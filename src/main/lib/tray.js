const { Menu, MenuItem, Tray } = require('electron');
const Broker = require('./Broker');

let tray;
const construct = () => {
  const menu = new Menu();
  menu.append(
    new MenuItem({
      label: 'control',
      click: () => {
        Broker.emit('show-control');
      },
    })
  );
  menu.append(
    new MenuItem({
      label: 'library',
      click: () => {
        Broker.emit('show-library');
      },
    })
  );
  menu.append(
    new MenuItem({
      label: 'player',
      click: () => {
        Broker.emit('show-player');
      },
    })
  );
  menu.append(
    new MenuItem({
      label: 'next',
      click: () => {
        Broker.emit('next');
      },
    })
  );
  menu.append(
    new MenuItem({
      label: 'stop',
      click: () => {
        Broker.emit('stop');
      },
    })
  );
  menu.append(
    new MenuItem({
      label: 'quit',
      click: () => {
        Broker.emit('quit');
      },
    })
  );

  tray = new Tray(`assets/icon.png`);
  tray.on('click', () => Broker.emit('show-control'));
  tray.setContextMenu(menu);
};

Broker.on('program:start', construct);
