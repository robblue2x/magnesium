const fs = require('fs');
const Broker = require('../Broker');
const windowManager = require('../window-manager');

const hacks = fs.readFileSync(`${__dirname}/../../hacks/get-links.js`).toString();

const cache = {};

const find = (query, callback) => {
  if (cache[query]) {
    console.log('from cache');
    callback(cache[query], query);
  } else {
    const videoUrl = `https://www.google.com/search?tbm=vid&q=${escape(query)}`;
    windowManager.load('search', videoUrl);

    Broker.once('searchresult', (result) => {
      if (result) {
        cache[query] = result;
      }
      callback(result, query);
    });
  }
};

Broker.once('program:start', () => {
  Broker.on('content:search:dom-ready', () => {
    windowManager.execute('search', hacks);
  });
});

module.exports = { find };
