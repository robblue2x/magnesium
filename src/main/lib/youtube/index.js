const playlist = require('./playlist');
const video = require('./video');

module.exports = {
  video,
  playlist,
};
