const fs = require('fs');
const Broker = require('../Broker');
const windowManager = require('../window-manager');

const hacks = fs.readFileSync(`${__dirname}/../../hacks/get-video-titles.js`).toString();

const find = (playlistUrl, callback) => {
  windowManager.load('playlist', playlistUrl);

  Broker.once('playlistresults', (list) => {
    callback(list);
  });
};

Broker.once('program:start', () =>
  Broker.on('content:playlist:dom-ready', () => {
    windowManager.execute('playlist', hacks);
  })
);

module.exports = { find };
