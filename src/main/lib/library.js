const electron = require('electron');
const fs = require('fs-extra');
const path = require('path');
const Broker = require('./Broker');
const youtube = require('./youtube');

let library = [];
let libraryFile = 'library.txt';
const history = [];

const send = () => {
  Broker.emit('library', library);
};

const loadLib = (color) => {
  const fileName = color ? `library-${color}.txt` : 'library.txt';
  libraryFile = path.join(electron.app.getPath('appData'), 'magnesium', fileName);

  if (process.env.SNAP_USER_COMMON) {
    libraryFile = path.join(process.env.SNAP_USER_COMMON, fileName);
  }

  fs.ensureFileSync(libraryFile);
  const loaded = fs
    .readFileSync(libraryFile)
    .toString()
    .split('\n')
    .filter((t) => t.length !== 0);
  console.log('libraryFile:', libraryFile);

  library = loaded.sort().map((a) => unescape(a).toLowerCase());

  send();
};

loadLib();

const save = () => {
  fs.writeFileSync(libraryFile, library.join('\n'));
  send();
};

const addList = (li) => {
  const list = li.filter((l) => l.length);
  if (list && list.length !== 0) {
    library = [...new Set(library.concat(list))].sort();
    Broker.emit('play', li[0]);
    save();
  }
};

const add = (track) => {
  if (track.startsWith('https://www.youtube.com/playlist?list=')) {
    youtube.playlist.find(track, addList);
  } else {
    addList([track.toLowerCase()]);
  }
};

const remove = (track) => {
  library = library.filter((a) => a !== track);
  save();
};

const next = () => {
  let t;
  do {
    t = library[Math.floor(Math.random() * library.length)];
  } while (history.includes(t));
  Broker.emit('stop');
  Broker.emit('play', t);
  if (library.length > 10) {
    history.unshift(t);
    if (history.length > 100 || history.length > library.length / 2) {
      history.pop();
    }
  }
};

library = [...new Set(library)].sort();
remove('');
remove('[deleted video]');
remove('[private video]');
save();

Broker.on('next', next);
Broker.on('remove', remove);
Broker.on('add', add);
Broker.on('content:library:dom-ready', send);
Broker.on('window:library:show', send);
Broker.on('library-switch', loadLib);
