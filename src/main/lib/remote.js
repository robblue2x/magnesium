const express = require('express');
const Broker = require('./Broker');

const PORT = process.env.REMOTE_PORT || 24305;

const httpRemote = express();

httpRemote.put('/api/1/next', (req, res) => {
  Broker.emit('next');
  res.status(200).end();
});

httpRemote.put('/api/1/stop', (req, res) => {
  Broker.emit('stop');
  res.status(200).end();
});

let playing = '';

Broker.on('playing', (t) => {
  playing = t;
});

httpRemote.get('/api/1/playing', (req, res) => {
  res.send({ playing });
});

let progress = 0;
Broker.on('progress', (p, d) => {
  progress = (p / d) * 100;
});

httpRemote.get('/api/1/progress', (req, res) => {
  res.send({ progress });
});

const server = httpRemote.listen(PORT, () => {
  console.log(`remote listening on port ${PORT}`);
});

server.on('error', (e) => {
  if (e.code === 'EADDRINUSE') {
    console.log(`magnesium already running, or something is using port ${PORT}`);
    process.exit();
  }
});

Broker.on('quit', () => {
  server.close();
});
