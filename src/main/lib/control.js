const { session } = require('electron');
const Broker = require('./Broker');
const youtube = require('./youtube');

let playing = '';

const play = (t) => {
  if (!t) {
    return;
  }
  playing = t;
  Broker.emit('playing', t, '');
  youtube.video.find(t, (videoUrl, query) => {
    if (query === playing) {
      if (videoUrl) {
        Broker.emit('playing', t, videoUrl);
        Broker.emit('load', videoUrl);
      } else {
        setTimeout(() => {
          Broker.emit('next');
        }, 1000);
      }
    }
  });
};

const stop = () => {
  Broker.emit('playing', 'Nothing playing', '');
  Broker.emit('load', `file://${__dirname}/../html/nothing-playing.html`);
  session.defaultSession.clearStorageData([], () => console.log('CLEAR clearStorageData'));
};

Broker.on('play', play);
Broker.on('stop', stop);
