const { EventEmitter } = require('events');
const { ipcMain } = require('electron');
const windowManager = require('./window-manager');

const Broker = new EventEmitter();

let last = '';
let lastCount = 0;

const log = (name, ...args) => {
  if (name !== last) {
    if (lastCount > 1) {
      console.log(`event - ${last} x(${lastCount})`);
    }
    lastCount = 0;
    if (name === 'library') {
      console.log(`event - ${name}`, args[0].length);
    } else {
      console.log(`event - ${name}`, ...args);
    }
    last = name;
  } else {
    lastCount += 1;
  }
};

ipcMain.on('ui', (evnt, name, ...args) => {
  Broker.emit(name, ...args);
});

Broker.internalEmit = Broker.emit;

Broker.emit = (name, ...args) => {
  log(name, ...args);
  windowManager.send('main', name, ...args);
  Broker.internalEmit(name, ...args);
};

windowManager.on('window', (name, ...args) => {
  log(`window:${name}`);
  Broker.internalEmit(`window:${name}`, ...args);
});

windowManager.on('content', (name, ...args) => {
  log(`content:${name}`);
  Broker.internalEmit(`content:${name}`, ...args);
});

module.exports = Broker;
