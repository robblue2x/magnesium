const { EventEmitter } = require('events');
const Window = require('./Window');

class WindowManager extends EventEmitter {
  constructor() {
    super();
    this.windows = [];
  }

  find(name) {
    const a = this.windows.find((b) => b.name === name);
    if (!a) {
      throw new Error('Window not found');
    }
    return a;
  }

  add(name, ...args) {
    const a = this.windows.find((b) => b.name === name);
    if (a) {
      throw new Error('window with that name exists');
    } else {
      const b = new Window(name, ...args);
      for (const event of ['show', 'hide']) {
        b.bw.on(event, (...winArgs) => {
          this.emit('window', `${name}:${event}`, ...winArgs);
        });
      }
      for (const event of ['dom-ready']) {
        b.bw.webContents.on(event, (...winArgs) => {
          this.emit('content', `${name}:${event}`, ...winArgs);
        });
      }
      this.windows.push(b);
    }
  }

  load(name, url) {
    this.find(name).load(url);
  }

  send(...args) {
    this.windows.forEach((a) => a.send(...args));
  }

  show(name) {
    this.find(name).show();
  }

  hide(name) {
    this.find(name).hide();
  }

  minimize(name) {
    this.find(name).minimize();
  }

  fullscreen(name, a) {
    this.find(name).fullscreen(a);
  }

  execute(name, code) {
    this.find(name).execute(code);
  }

  insertCSS(name, code) {
    this.find(name).insertCSS(code);
  }

  close(name) {
    this.find(name).close();
  }

  closeAll() {
    console.log('WM:closeAll');
    this.windows.forEach((a) => a.close());
  }
}

const windowManager = new WindowManager();

module.exports = windowManager;
