const { BrowserWindow } = require('electron');

class Window {
  constructor(name, width, height, show) {
    this.name = name;
    this.closed = false;
    const icon = 'assets/icon.png';
    this.bw = new BrowserWindow({
      show,
      width,
      height,
      icon,
      autoHideMenuBar: true,
      webPreferences: {
        nodeIntegration: true,
        contextIsolation: false,
      },
    });
    this.bw.on('close', (event) => {
      event.preventDefault();
      this.bw.hide();
    });
    // this.bw.webContents.openDevTools();
  }

  load(url) {
    this.bw.webContents.loadURL(url);
  }

  send(...args) {
    if (this.closed) return;
    this.bw.webContents.send(...args);
  }

  show() {
    this.bw.show();
  }

  hide() {
    this.bw.hide();
  }

  minimize() {
    this.bw.minimize();
  }

  fullscreen(a) {
    this.bw.setFullScreen(a);
  }

  execute(code) {
    this.bw.webContents.executeJavaScript(code);
  }

  insertCSS(code) {
    this.bw.webContents.insertCSS(code);
  }

  close() {
    if (this.closed) return;
    this.bw.removeAllListeners('close');
    this.bw.close();
    this.closed = true;
  }
}

module.exports = Window;
