const { app, session } = require('electron');
const process = require('process');
const path = require('path');
const Broker = require('./lib/Broker');
const windowManager = require('./lib/window-manager');

const workDir = path.dirname(process.argv[0]);
const binName = path.basename(process.argv[0]);
console.log('workDir', workDir);
console.log('binName', binName);
if (binName !== 'electron') process.chdir(workDir);

const PROXY = process.env.PROXY || '';

require('./lib/remote');
require('./lib/control');
require('./lib/library');
require('./lib/player');
require('./lib/mediakeys');
require('./lib/tray');

const blocklist = [
  '*.doubleclick.net',
  '*.googleadservices.com',
  '*.googlesyndication.com',
  's.youtube.com',
  '*.ytimg.com',
  '*.ggpht.com',
  'adservice.google.*',
];

const rules = blocklist.reduce((a, v) => `${a}MAP ${v} 0.0.0.0, `);

app.commandLine.appendSwitch('incognito');
app.commandLine.appendSwitch('host-rules', rules);
app.commandLine.appendSwitch('autoplay-policy', 'no-user-gesture-required');
app.commandLine.appendSwitch('disable-gpu-compositing');

const exit = () => {
  windowManager.closeAll();
  app.quit();
};

const onBeforeRequest = ({ url }, callback) => {
  if (url.includes('https://www.youtube.com/api/stats')) {
    callback({ cancel: true });
  } else if (url.includes('https://www.youtube.com/youtubei/v1/log')) {
    callback({ cancel: true });
  } else if (url.includes('https://www.youtube.com/ptracking')) {
    callback({ cancel: true });
  } else if (url.includes('https://adservice.google.com')) {
    callback({ cancel: true });
  } else if (url.includes('https://googleads.g.doubleclick.net')) {
    callback({ cancel: true });
  } else if (url.includes('https://www.google.co.uk/pagead')) {
    callback({ cancel: true });
  } else if (url.includes('https://www.youtube.com/pagead')) {
    callback({ cancel: true });
  } else {
    callback({ cancel: false });
  }
};

const start = () => {
  session.defaultSession.webRequest.onBeforeRequest(
    {
      urls: [
        'https://www.youtube.com/*',
        'https://adservice.google.com/*',
        'https://googleads.g.doubleclick.net/*',
        'https://www.google.co.uk/*',
      ],
    },
    onBeforeRequest
  );

  windowManager.add('control', 480, 100, true);
  console.log(`file://${__dirname}/../../ui/control.html`);
  windowManager.load('control', `file://${__dirname}/../../ui/control.html`);
  Broker.on('show-control', () => {
    windowManager.show('control');
  });

  windowManager.add('library', 800, 800, false);
  windowManager.load('library', `file://${__dirname}/../../ui/library.html`);
  Broker.on('show-library', () => {
    windowManager.show('library');
  });

  windowManager.add('player', 640, 480, false);
  windowManager.add('search', 200, 100, false);
  windowManager.add('playlist', 200, 100, false);

  Broker.emit('program:start');
  Broker.on('quit', exit);
  process.on('SIGINT', exit);

  Broker.once('library', () => {
    Broker.emit('next');
  });
};

const preStart = () => {
  console.log('Setting proxy');
  session.defaultSession.setProxy({ proxyRules: PROXY }).then(() => {
    console.log('Proxy set, starting app');
    start();
  });
};

app.on('ready', preStart);

app.on('window-all-closed', app.quit);
