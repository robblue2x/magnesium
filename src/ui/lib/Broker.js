const { EventEmitter } = require('events');
const { ipcRenderer } = require('electron');

const Broker = new EventEmitter();

Broker.internalEmit = Broker.emit;

Broker.emit = (name, ...args) => {
  console.log(`event - ${name}`, ...args);
  ipcRenderer.send('ui', name, ...args);
  return Broker;
};

ipcRenderer.on('main', (e, name, ...args) => {
  console.log(`emit - ${name}`, ...args);
  Broker.internalEmit(name, ...args);
});

module.exports = Broker;
