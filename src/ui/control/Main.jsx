import React from 'react';
import { clipboard, shell } from 'electron';
import { Header, Segment } from 'semantic-ui-react';
import Broker from '../lib/Broker';

import MgButton from './MgButton';

export default class Main extends React.Component {
  constructor() {
    super();
    this.state = { playing: '', url: '', duration: 0, progress: 0 };
    this.updatePlaying = (playing, url) => {
      this.setState({ playing, url, duration: 0, progress: 0 });
    };
    this.updateProgress = (progress, duration) => {
      this.setState({ progress, duration });
    };
  }

  componentDidMount() {
    Broker.on('playing', this.updatePlaying);
    Broker.on('progress', this.updateProgress);
  }

  componentWillUnmount() {
    Broker.removeListener('playing', this.updatePlaying);
    Broker.removeListener('progress', this.updateProgress);
  }

  render() {
    const { duration, playing } = this.state;
    let { progress } = this.state;
    const { url } = this.state;

    const percent = duration <= 0 ? 0 : (100 * progress) / duration;

    const noPadding = { padding: 0, margin: 0 };

    const progressBarStyle = {
      background: 'grey',
      position: 'fixed',
      bottom: 0,
      left: 0,
      right: 0,
      height: '10px',
      opacity: 0.5,
    };

    const progressStyle = {
      background: 'grey',
      position: 'fixed',
      bottom: 0,
      left: 0,
      height: '10px',
      width: `${percent}%`,
    };
    const progressBarClick = (e) => {
      if (!duration) {
        return;
      }
      const s = (e.clientX / window.innerWidth) * duration;
      Broker.emit('seek', s);
      progress = s;
      this.setState({ progress });
    };

    const seek = (delta) => {
      let t = progress + delta;
      t = t < 0 ? 0 : t;
      t = t > duration ? duration : t;
      progress = t;
      this.setState({ progress });
      Broker.emit('seek', t);
    };

    const openHelp = () => {
      shell.openExternal('https://robblue2x.gitlab.io/magnesium/');
    };

    return (
      <Segment color="black" inverted style={{ position: 'fixed', top: -10, bottom: -10, left: -10, right: -10 }}>
        <Segment inverted basic textAlign="center" style={noPadding}>
          <MgButton icon="stop" onClick={() => Broker.emit('stop')} />
          <MgButton icon="step forward" onClick={() => Broker.emit('next')} />
          <MgButton icon="backward" onClick={() => seek(-15)} />
          <MgButton icon="forward" onClick={() => seek(15)} />
          <MgButton icon="remove" onClick={() => Broker.emit('remove', playing).emit('next')} />
          <MgButton icon="list" onClick={() => Broker.emit('show-library')} />
          <MgButton icon="film" onClick={() => Broker.emit('show-player')} />
          <MgButton icon="desktop" onClick={() => Broker.emit('show-tvmode')} />
          <MgButton icon="share" onClick={() => clipboard.writeText(url)} />
          <MgButton icon="help" onClick={openHelp} />
          <MgButton icon="sign-out" onClick={() => Broker.emit('quit')} />
        </Segment>

        <Segment inverted basic textAlign="center" style={noPadding}>
          <Header as="h2" style={{ textTransform: 'capitalize' }}>
            {playing}
          </Header>
        </Segment>

        <div style={progressStyle} />
        <div style={progressBarStyle} onClick={progressBarClick} />
      </Segment>
    );
  }
}
