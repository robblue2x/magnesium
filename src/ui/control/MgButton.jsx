import React from 'react';

import { Button, Icon } from 'semantic-ui-react';

const MgButton = (props) => {
  const { icon, title, onClick } = props;
  return (
    <Button icon compact title={title} color="black" onClick={onClick}>
      <Icon name={icon} />
    </Button>
  );
};

export default MgButton;
