import React from 'react';
import ReactList from 'react-list';
import { Grid, Button, Icon, Input, Segment } from 'semantic-ui-react';
import Broker from '../lib/Broker';
import Track from './Track';

const filterShown = (search, library) => {
  if (!search || search.length === 0) {
    return library;
  }
  const s = search.toLowerCase();
  return library.filter((t) => t.toLowerCase().indexOf(s) !== -1);
};

export default class Main extends React.Component {
  constructor() {
    super();
    this.state = { library: [], showing: [], nowPlaying: '', search: '', selectedLib: undefined };
    this.updateLibrary = (library) => {
      const { search } = this.state;
      this.setState({ library, showing: filterShown(search, library) });
    };

    this.updateNowPlaying = (nowPlaying) => {
      this.setState({ nowPlaying });
    };
  }

  componentDidMount() {
    Broker.on('library', this.updateLibrary);
    Broker.on('playing', this.updateNowPlaying);
  }

  componentWillUnmount() {
    Broker.removeListener('library', this.updateLibrary);
    Broker.removeListener('playing', this.updateNowPlaying);
  }

  updateSearch(search) {
    const { library } = this.state;
    this.setState({ search, showing: filterShown(search, library) });
  }

  render() {
    const list = [];
    const { nowPlaying, showing, selectedLib, search } = this.state;
    for (const track of showing) {
      list.push(<Track playing={nowPlaying === track} track={track} key={list.length} />);
    }

    const onKeyPress = (e) => {
      if (e.which === 13) {
        Broker.emit('add', e.target.value);
        this.updateSearch('');
      }
    };

    const flexBoxStyle = {
      position: 'absolute',
      display: 'flex',
      flexDirection: 'column',
      margin: 0,
      padding: 0,
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
    };

    const unselected = {
      borderColor: 'black',
      borderWidth: '8px',
      borderStyle: 'solid',
    };

    const switchLib = (library) => {
      Broker.emit('library-switch', library);
      this.setState({ selectedLib: library });
    };

    const itemRenderer = (index, key) => {
      const track = showing[index];
      return <Track playing={nowPlaying === track} track={track} key={key} />;
    };

    const colors = [undefined, 'red', 'orange', 'yellow', 'green', 'teal', 'blue', 'violet', 'pink', 'brown'];

    return (
      <Segment basic color="black" inverted style={flexBoxStyle}>
        <Segment basic color="black" inverted style={{ margin: 0 }}>
          <Grid columns="equal" style={{ background: 'white' }}>
            {colors.map((a) => (
              <Grid.Column
                key={a || 'white'}
                color={a}
                onClick={() => switchLib(a)}
                style={selectedLib === a ? {} : unselected}
              />
            ))}
          </Grid>
        </Segment>
        <Segment basic color="black" inverted>
          <Button floated="right" icon circular color="black" inverted onClick={() => this.updateSearch('')}>
            <Icon name="remove" />
          </Button>
          <Input
            fluid
            placeholder="Artist and track name / Youtube playlist url"
            type="text"
            value={search}
            onKeyPress={onKeyPress}
            onChange={(e) => this.updateSearch(e.target.value)}
          />
        </Segment>
        <div style={{ overflowX: 'hidden' }}>
          <ReactList itemRenderer={itemRenderer} length={showing.length} type="uniform" />
        </div>
      </Segment>
    );
  }
}
