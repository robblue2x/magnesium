import React from 'react';
import { Button, Header, Icon, Segment } from 'semantic-ui-react';
import Broker from '../lib/Broker';

const Track = (props) => {
  const { playing, track } = props;

  const outerStyle = { 'padding-top': '4px', 'padding-bottom': '4px', 'margin-top': 0, 'margin-bottom': 0 };

  return (
    <Segment inverted color="black" style={outerStyle}>
      <Button icon color="black" floated="left" onClick={() => Broker.emit('play', track)}>
        <Icon name="play" />
      </Button>
      <Button icon color="black" floated="left" onClick={() => Broker.emit('remove', track)}>
        <Icon name="remove" />
      </Button>
      <Header color={playing ? 'blue' : 'grey'} style={{ 'margin-top': '0.5em' }}>
        <div style={{ textOverflow: 'ellipsis', whiteSpace: 'nowrap', textTransform: 'capitalize' }}>{track}</div>
      </Header>
    </Segment>
  );
};

export default Track;
